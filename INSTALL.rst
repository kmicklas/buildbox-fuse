Build and installation instructions for BuildBox
================================================

These instructions are written for Debian 9. Other platforms will be added after we are able to test them.

Dependencies
------------

BuildBox has several dependencies, most of which can be obtained via apt on Debian 9. Install these first::

  sudo apt install udev build-essential autoconf cmake libtool pkg-config libc-ares-dev libssl-dev curl make g++ unzip git ninja-build python3-pip uuid-dev

Next, Meson is required. The version of Meson in Debian 9 is too old to build BuildBox, so install it via pip3 instead, using::

  sudo pip3 install meson

or::

  pip3 install --user meson

If you use the second option, ensure `~/.local/bin` is on your path.

Next, protobuf, grpc and fuse have to be installed from source. This is described in the following sections.

protobuf
--------

The following instructions should build protobuf v3.6.0 from source::

  git clone https://github.com/google/protobuf.git
  cd protobuf
  git checkout v3.6.0
  git submodule update --init --recursive
  ./autogen.sh
  ./configure
  make -j 4
  sudo make install
  sudo ldconfig

grpc
----

Follow these instructions to build grpc v1.14.1 from source::

  git clone https://github.com/grpc/grpc.git
  cd grpc
  git checkout v1.14.1
  git submodule update --init
  make -j 4
  sudo make install

libfuse
-------

The latest release of libfuse 3 should be installed, currently 3.2.5::

  wget https://github.com/libfuse/libfuse/releases/download/fuse-3.2.5/fuse-3.2.5.tar.xz
  tar -xf fuse-3.2.5.tar.xz
  cd fuse-3.2.5
  mkdir build
  meson --libdir /usr/local/lib build
  ninja -C build
  sudo ninja -C build install
  sudo ldconfig

Finally, we can now install buildbox itself:

buildbox-fuse
-------------

If you don't already have buildbox-fuse cloned, do so::

  git clone https://gitlab.com/BuildGrid/buildbox/buildbox-fuse.git
  cd buildbox-fuse

Now run::
  mkdir -p build && cd build
  cmake ..
  make
  sudo make install

After this, running `buildbox` should show a list of options.
